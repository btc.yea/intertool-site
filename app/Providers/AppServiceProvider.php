<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Categories;
use App\Slider;
use App\Contacts;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::composer(['front', 'front.partials.categories-modal'], function ($view) {
            $categories = Categories::where('parent_id', '1')->whereRaw('(select count(*) from categories as cat where cat.parent_id = categories.id limit 1) > 0')->get();
            $subcategories_ids = Categories::select('parent_id')
                ->where('parent_id', '>', '1')
                ->distinct()
                ->get()
                ->pluck('parent_id');
            $subcategories = [];
            foreach ($subcategories_ids as $id) {
                $subcategories[$id] = Categories::where('parent_id', $id)->get()->toArray();
            }
            $images = Slider::get();
            $contacts = Contacts::get();


            $view->with(compact(
                'categories',
                'subcategories',
                'contacts'
            ));
        });
    }
}
