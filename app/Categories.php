<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';
    protected $fillable = ['id', 'parent_id', 'name', 'image'];
    public $timestamps = false;
}
