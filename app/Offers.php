<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    protected $table = 'offers';
    protected $fillable = ['id', 'category_id', 'name', 'price', 'type_prefix', 'params', 'model', 'available'];
    public $timestamps = false;
}
