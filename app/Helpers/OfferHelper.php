<?php

namespace App\Helpers;

use App\Rating;

class OfferHelper
{
    public static function getAvarageRating(int $id)
    {
        return Rating::select('rating')->where('product_id', $id)->get()->avg('rating'); 
    }
}
