<?php

namespace App\Helpers;

use App\Offers;
use App\ArchiveStorage;

class ArchiveHelper
{
    public static function getOfferById(int $id)
    {
        return Offers::select('name', 'model')->where('id', $id)->first(); 
    }

    public static function getStorageNameById(int $id)
    {
        return ArchiveStorage::select('name')->where('id', $id)->first()->name; 
    }
}
