<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Categories;
use App\Contacts;
use App\Offers;
use App\Rating;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function index($id)
    {
        $product = Offers::where('id', $id)->first();
        $currentCategory = Categories::where('id', $product->category_id)->first();
        $title = $product->name.' - купить онлайн';
        $params = json_decode($product->params, true);

        $productImages = [];
        $filename = 'storage/offers/'.$product->model;
        $filenameOfiginal = $filename;
        $i = 0;
        while (File::exists($filename.'.jpg')) {
            $i++;
            $productImages[] = urlencode($filename.'.jpg');
            $filename = $filenameOfiginal.'+'.$i;
        }

        $rating = Rating::select('rating')->where('product_id', $product->id)->get();
        $ratingCount = $rating->count();
        $avarageRating = $rating->avg('rating') ?? 5;

        return view('front.product.index', compact(
            'title',
            'product',
            'productImages',
            'currentCategory',
            'params',
            'ratingCount',
            'avarageRating'
        ));
    }

    public function mainSearchAutocomplete($search)
    {
        $results = Offers::select('name', 'model', 'id')->where('name', 'like', '%'.$search.'%')->limit(20)->get();
        $items = [];
        foreach ($results as $res) {
            if (\Illuminate\Support\Facades\File::exists('storage/offers/'.$res->model.'.jpg')) {
                $items[] = [
                    'name' => $res->name,
                    'image' => asset('storage/offers/'.$res->model.'.jpg'),
                    'id' => $res->id
                ];
            }

            if (count($items) == 8) {
                break;
            }
        }
        return response()->json($items);
    }

    public function insertRating(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'product_id' => 'required',
            'rating' => 'required|min:0.5'
        ]);
    
        if ($validator->fails()) {
            $responseArr = [];
            $responseArr['success'] = false;
            $responseArr['errors'] = $validator->errors();;
            return response()->json($responseArr);
        }

        $rating = new Rating;
        $rating->user_id = \Auth::user()->id;
        $rating->product_id = $request->input('product_id');
        $rating->rating = $request->input('rating');
        $rating->comment = $request->input('comment');
        $rating->save();

        return response()->json(['success' => true]);
    }

    public function getRatingComments($id, Request $request)
    {
        $limit = $request->input('limit') ?? 4; 

        $results = Rating::where('product_id', $id)->limit($limit)->orderBy('id', 'DESC')->get();
        $comments = [];
        foreach ($results as $res) {
            $dt = new Carbon($res->created_at);
            $date = $dt->format('d-m-Y \в h:i');
            $comments[] = [
                'user_name' => \App\User::find($res->user_id)->name,
                'rating' => $res->rating,
                'comment' => $res->comment,
                'id' => $res->id,
                'date' => $date
            ];
        }
        return response()->json($comments);
    }

    public function isRated($id)
    {
        $response = 0;

        if (! \Auth::guest()) {
            $result = Rating::select('id')
                ->where('product_id', $id)
                ->where('user_id', \Auth::user()->id)
                ->first();
            
            $response = (int)($result->count() > 0);
        }

        return response()->json($response);
    }
}
