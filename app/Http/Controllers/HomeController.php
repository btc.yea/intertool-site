<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Offers;

class HomeController extends Controller
{
    public function index()
    {
        $title = 'Официальный интернет магазин';
        $images = Slider::get();

        $stocks = Offers::select(
                'offers.id as id', 
                'offers.model as model',
                'offers.name as name',
                'offers.price as price',
                'stock.new_price as new_price'
            )
            ->rightJoin('stock', 'offers.id', '=', 'stock.offer_id')
            ->paginate(15);

        return view('front.home', compact(
            'title',
            'images',
            'stocks'
        ));
    }
}
