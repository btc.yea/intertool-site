<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Offers;

class CategoriesController extends Controller
{
    public function index($id)
    {
        $currentCategory = Categories::where('id', $id)->first();
        $title = $currentCategory->name.' - Купить';

        $offers = Offers::where('category_id', $id)->get();

        return view('front.category', compact(
            'title',
            'offers',
            'currentCategory'
        ));
    }

    public function search(Request $request)
    {
        $title = $request->input('search').' - Поиск';
        $search = $request->input('search');

        if (!empty($search)) {
            $offers = Offers::where('name', 'like', '%'.$search.'%');
            $offers = $offers->get();
        } else {
            return redirect()->route('home');
        }
        
        return view('front.category', compact(
            'title',
            'offers',
            'search'
        ));
    }
}
