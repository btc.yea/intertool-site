<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Offers;
use App\Stock;
use App\Categories;
use Session;
use Redirect;
use App\User;

class ProductsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        Session::flash('backUrl', \Request::fullUrl());
        $title = 'Админ-панель - товары';
        $products = Offers::paginate($user->products_per_page);
        $categories = Categories::where('parent_id', '>', '1')->get();
        return view('admin.products.index', compact(
            'title',
            'products',
            'categories',
            'user'
        ));
    }

    public function changeProductsPerPage(Int $perPage)
    {
        User::where('id', Auth::id())->update(['products_per_page' => $perPage]);
        return redirect()->back();
    }

    public function search(Request $request)
    {
        $user = Auth::user();
        Session::flash('backUrl', \Request::fullUrl());
        $title = 'Админ-панель - товары';
        $search = $request->input('search');
        $categories = Categories::where('parent_id', '>', '1')->get();
        $cat_id = $request->input('cat_id');

        if (!empty($search) || $cat_id > 1) {
            $products = Offers::where('name', 'like', '%'.$search.'%');
            
            if ($cat_id > 1) {
                $products->where('category_id', $cat_id);
            }
            $products = $products->paginate($user->products_per_page);
        } else {
            return redirect()->route('admin.products');
        }
        
        return view('admin.products.index', compact(
            'title',
            'products',
            'search',
            'categories',
            'cat_id',
            'user'
        ));
    }

    public function edit(Request $request, $id)
    {
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }
        $product = Offers::where('id', $id)->first();
        $title = 'Админ-панель - ' . $product->name;
        $params = json_decode($product->params, true);

        if ($request->isMethod('post')) {
            $changes = [];
            $changes['name'] = $request->input('name');
            $changes['model'] = $request->input('model');
            $changes['about'] = $request->input('about');
            $params = [];
            for ($i=0; $i < $request->input('params_count'); $i++) { 
                if (!empty($request->input('param_name'.$i)) && !empty($request->input('param_value'.$i))) {
                    $params[] = [
                        'name' => [$request->input('param_name'.$i)],
                        'value' => $request->input('param_value'.$i),
                        'unit' => [$request->input('param_unit'.$i)]
                    ];
                }
            }
            $changes['params'] = json_encode($params, JSON_UNESCAPED_UNICODE);

            // $file = $request->file('image');
            // if (!empty($file)) {
            //     $randomName = time();
            //     $destinationPath = 'categories';
            //     $extension = $file->getClientOriginalExtension();
            //     $filename=$randomName.'_category.'.$extension;
            //     $uploadSuccess = $request->file('image')->move(public_path($destinationPath), $filename);
            //     $changes['image'] = $filename;
            // }

            // dd($changes);
            Offers::where('id', $id)->update($changes);

            return ($url = Session::get('backUrl')) 
            ? Redirect::to($url)->with('success', 'Товар успешно изменён')
            : Redirect::route('admin.products')->with('success', 'Товар успешно изменён');

        }

        return view('admin.products.edit', compact(
            'title',
            'id',
            'product',
            'params'
        ));
    }

    public function remove($id)
    {
        $category = Categories::where('id', $id);
        $category->delete();
        return redirect()->back()->with('success', 'Категория успешно удаленена.');
    }

    public function add(Request $request)
    {
        $title = 'Админ-панель - добавить категорию';
        $categories = Categories::where('parent_id', '1')->get();

        if ($request->isMethod('post')) {
            $parentId = $request->input('parent_id');
            $name = $request->input('name');
            $file = $request->file('image');
            if (!empty($file)) {
                $randomName = time();
                $destinationPath = 'categories';
                $extension = $file->getClientOriginalExtension();
                $filename=$randomName.'_category.'.$extension;
                $uploadSuccess = $request->file('image')->move(public_path($destinationPath), $filename);
                $image = $filename;
            }

            Categories::create([
                'parent_id' => $parentId,
                'name'      => $name,
                'image'     => $image
            ]);

            return redirect()->route('admin.categories')->with('success', 'Категория успешно создана.');
        }

        return view('admin.categories.add', compact(
            'title',
            'categories'
        ));
    }

    public function stock()
    {
        $title = 'Админ-панель - Акции';
        $products = Offers::whereRaw('(select count(*) from stock where offers.id = offer_id) = 1')->paginate(15);
        
        return view('admin.stock.index', compact(
            'title',
            'products'
        ));
    }

    public function addStock(Request $request, $id)
    {
        $title = 'Админ-панель - Акции';
        $offer = Offers::where('id', $id)->first();
        $stock = Stock::where('offer_id', $id)->first();

        if ($request->isMethod('post')) {
            if ($stock) {
                Stock::where('id', $stock->id)->update(['new_price' => $request->input('new_price')]);
            } else {
                $new_stock = new Stock;
                $new_stock->offer_id = $id;
                $new_stock->new_price = $request->input('new_price');
                $new_stock->save();
            }
            return redirect()->route('admin.stock')->with('success', 'Акция добавлена.');
        }
        
        return view('admin.stock.add', compact(
            'title',
            'offer',
            'id',
            'stock'
        ));
    }
    public function removeStock($id)
    {
        $stock = Stock::where('offer_id', $id);
        $stock->delete();
        return redirect()->route('admin.stock')->with('success', 'Акция успешно удаленена.');
    }
}
