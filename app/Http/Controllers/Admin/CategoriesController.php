<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use App\Categories;

class CategoriesController extends Controller
{
    public function index()
    {
        $title = 'Админ-панель - каталог';
        $categories = Categories::where('parent_id', '1')->paginate(15);
        
        return view('admin.categories.index', compact(
            'title',
            'categories'
        ));
    }

    public function category($id)
    {
        $title = 'Админ-панель - ' . Categories::where('id', $id)->first()->name;
        $categories = Categories::where('parent_id', $id)->paginate(15);

        return view('admin.categories.category', compact(
            'title',
            'categories'
        ));
    }

    public function edit(Request $request, $id)
    {
        $catName = Categories::where('id', $id)->first()->name;
        $title = 'Админ-панель - ' . $catName;

        if ($request->isMethod('post')) {
            $changes = [];
            $changes['name'] = $request->input('name');

            $file = $request->file('image');
            if (!empty($file)) {
                $randomName = time();
                $destinationPath = 'categories';
                $extension = $file->getClientOriginalExtension();
                $filename=$randomName.'_category.'.$extension;
                $uploadSuccess = $request->file('image')->move(public_path($destinationPath), $filename);
                $changes['image'] = $filename;
            }

            Categories::where('id', $id)->update($changes);

            return redirect()->route('admin.categories')->with('success', 'Категория успешно изменена.');
        }

        return view('admin.categories.edit', compact(
            'title',
            'id',
            'catName'
        ));
    }

    public function remove($id)
    {
        $category = Categories::where('id', $id);
        $category->delete();
        return redirect()->back()->with('success', 'Категория успешно удаленена.');
    }

    public function toggle($id)
    {
        $category = Categories::where('id', $id)->first();
        Categories::where('id', $id)->update(['is_hidden' => !$category->is_hidden]);
        $words = $category->is_hidden ? 'теперь отображается' : 'успешно скрыта';
        return redirect()->back()->with('success', 'Категория '.$words.'.');
    }

    public function add(Request $request)
    {
        $title = 'Админ-панель - добавить категорию';
        $categories = Categories::where('parent_id', '1')->get();

        if ($request->isMethod('post')) {
            $parentId = $request->input('parent_id');
            $name = $request->input('name');
            $file = $request->file('image');
            if (!empty($file)) {
                $randomName = time();
                $destinationPath = 'categories';
                $extension = $file->getClientOriginalExtension();
                $filename=$randomName.'_category.'.$extension;
                $uploadSuccess = $request->file('image')->move(public_path($destinationPath), $filename);
                $image = $filename;
            }

            Categories::create([
                'parent_id' => $parentId,
                'name'      => $name,
                'image'     => $image
            ]);

            return redirect()->route('admin.categories')->with('success', 'Категория успешно создана.');
        }

        return view('admin.categories.add', compact(
            'title',
            'categories'
        ));
    }
}
