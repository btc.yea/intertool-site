<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use App\Offers;
use App\ArchiveStorage;
use App\Archive;
use Symfony\Component\HttpFoundation\Response;

class ArchiveController extends Controller
{
    public function index()
    {
        $title = 'Админ-панель - Товары в наличии';
        $archive = Archive::paginate(15);
        
        return view('admin.archive.index', compact(
            'title',
            'archive'
        ));
    }

    public function add(Request $request)
    {
        if ($request->isMethod('post')) {
            $storage_id = $request->input('storage_id');
            $name = $request->input('name');
            $barcode = $request->input('barcode');
            $count = $request->input('count');

            $offer = Offers::select('id')->where('name', $name)->first() ?? null;
            $offer_id = $offer ? $offer->id : null;

            if ($offer_id) {
                if (!empty($storage_id) && !empty($barcode) && !empty($count)) {
                    Offers::where('id', $offer_id)
                        ->update(['barcode' => $barcode]);
                    $archive = new Archive;
                    $archive->storage_id = $storage_id;
                    $archive->offer_id = $offer_id;
                    $archive->count = $count;
                    $archive->save();
                    
                    return redirect()->route('admin.archive')->with('success', 'Товар добавлен.');
                }
            } else {
                return redirect()->back()->with('error', 'Такого товара нет в базе.');
            }
            
            return redirect()->back()->with('error', 'Не все поля заполнены.');
        }

        $title = "Админ-панель - Добавить наличие товара";
        
        $storages = ArchiveStorage::get();

        return view('admin.archive.add', compact(
            'title',
            'storages'
        ));
    }

    public function removeArchiveEntry($id)
    {
        $entry = Archive::where('id', $id);
        $entry->delete();
        return redirect()->route('admin.archive')->with('success', 'Запись удалена.');
    }

    public function addStorage(Request $request)
    {
        if ($request->isMethod('post')) {
            $type = $request->input('type');
            $name = $request->input('name');
            $address = $request->input('address');
            
            if (!empty($type) && !empty($name) && !empty($address)) {
                $archiveStorage = new ArchiveStorage;
                $archiveStorage->type = $type;
                $archiveStorage->name = $name;
                $archiveStorage->address = $address;
                $archiveStorage->save();
                
                return redirect()->route('admin.archive')->with('success', 'Место хранения/продажи успешно добавлено.');
            }
            
            return redirect()->back()->with('error', 'Не все поля заполнены.');
        }

        $title = "Админ-панель - Добавить место хранения товара";

        return view('admin.archive.add-storage', compact(
            'title'
        ));
    }

    public function productNameAutocomplete($search)
    {
        $results = Offers::select('name')->where('name', 'like', '%'.$search.'%')->orWhere('model', 'like', '%'.$search.'%')->limit(10)->get();
        $items = [];
        foreach ($results as $res) {
            $items[] = $res->name;
        }
        return response($items);
    }

    public function productNameByBarcode(int $code)
    {
        $result = Offers::select('name')->where('barcode', $code)->first();
        return response($result->name ?? '');
    }
    
}
