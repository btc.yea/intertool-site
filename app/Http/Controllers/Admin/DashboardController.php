<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use App\Slider;
use App\Contacts;

class DashboardController extends Controller
{
    public function index()
    {
        $title = 'Админ-панель';
        $images = Slider::get();
        $contacts = Contacts::get();
        
        return view('admin.dashboard', compact(
            'title',
            'images',
            'contacts'
        ));
    }

    public function addImage()
    {
        $title = "Админ-панель - Добавить изображение в слайдер";


        return view('admin.slider.add', compact(
            'title'
        ));
    }

    public function insertImage(Request $request)
    {
        $file = $request->file('image');
        $randomName = time();
        $destinationPath = 'slider';
        $extension = $file->getClientOriginalExtension();
        $filename=$randomName.'_slider.'.$extension;
        $uploadSuccess = $request->file('image')->move(public_path($destinationPath), $filename);
        Slider::create([
            'image' => $filename,
        ]);
    
        return redirect()->route('admin.dashboard')->with('success', 'Изображение успешно загружено.');
    }

    public function removeImage($id)
    {
        $image = Slider::where('id', $id);
        $filename = 'slider/'.$image->first()->image;
        File::delete($filename);
        $image->delete();
        return redirect()->route('admin.dashboard')->with('success', 'Изображение успешно удалено.');
    }

    public function addContact()
    {
        $title = "Админ-панель - Добавить контакт";


        return view('admin.contacts.add', compact(
            'title'
        ));
    }

    public function insertContact(Request $request)
    {
        $number = $request->input('number');
        $worktime = $request->input('worktime') ?? '';
        
        $contact = new Contacts;
        $contact->number = $number;
        $contact->worktime = $worktime;
        $contact->save();
    
        return redirect()->route('admin.dashboard')->with('success', 'Номер добавлен успешно.');
    }

    public function removeContact($id)
    {
        $contact = Contacts::where('id', $id);
        $contact->delete();
        return redirect()->route('admin.dashboard')->with('success', 'Номер успешно удаленён.');
    }

    public function editContact(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            Contacts::where('id', $id)->update([
                'number' => $request->input('number'),
                'worktime' => $request->input('worktime') ?? ''
            ]);
            return redirect()->route('admin.dashboard')->with('success', 'Номер успешно изменён.');
        }
        
        $tel = Contacts::where('id', $id)->firstOrFail();
        $title = 'Редактировать контакт';
    
        return view('admin.contacts.edit', compact(
            'title',
            'id',
            'tel'
        ));
    }
}
