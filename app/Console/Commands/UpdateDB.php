<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Categories;
use App\Offers;

class UpdateDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:db {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import database from xml';

    private $reader;
	private $tag;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!empty($this->argument('path'))) {
            $this->reader = simplexml_load_file($this->argument('path'));
            
            // $categories = $this->reader->shop->categories->category;
            // $cleanCategories = [];

            // foreach ($categories as $cat) {
            //     $id             = (string) $cat->attributes()->id;
            //     $parentId       = ((string) $cat->attributes()->parentId != '') ? (string) $cat->attributes()->parentId : 0;
            //     $categoryName   = (string) $cat;
            //     $cleanCategories[] = [
            //         'id'        => $id,
            //         'parent_id' => $parentId,
            //         'name'      => $categoryName,
            //     ];
            // }

            $offers = $this->reader->shop->offers->offer;
            foreach ($offers as $offer) {
                $available      = (string) $offer->attributes()->available;
                $name           = (string) $offer->name;
                $price          = (string) $offer->price;
                $categoryId     = (string) $offer->categoryId;
                $typePrefix     = (string) $offer->typePrefix;
                $params         = $offer->param;
                $model          = $offer->vendorCode;
                $strpos         = intval(mb_strpos($model, ".")) ? mb_strpos($model, ".") : (mb_strlen($model));
                $model          = mb_substr($model, 0, $strpos); 

                $cleanParams = [];
                foreach ($params as $param) {
                    $unit   = $param->attributes()->unit;
                    $paramName   = $param->attributes()->name;
                    $value  = (string) $param;
                    $cleanParams[] = [
                        'name' => $paramName,
                        'value' => $value,
                        'unit' => $unit,
                    ];
                }
                $cleanParams = json_encode($cleanParams, JSON_UNESCAPED_UNICODE);

                Offers::create([
                    'available' => $available,
                    'name' => $name,
                    'price' => $price,
                    'category_id' => $categoryId,
                    'type_prefix' => $typePrefix,
                    'params' => $cleanParams,
                    'model' => $model,
                ]);
            }

            // foreach ($cleanCategories as $cat) {
            //     $data = [];
            //     foreach ($cat as $col => $value) {
            //         $data[$col] = $value;
            //     }

            //     Categories::create($data);
            // }
        }
    }
}
