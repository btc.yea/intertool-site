<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchiveStorage extends Model
{
    protected $table = 'archive_storages';
    protected $fillable = ['id', 'name', 'type', 'address'];
    public $timestamps = false;
}
