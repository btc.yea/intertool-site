<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'rating';
    protected $fillable = ['id', 'product_id', 'user_id', 'rating', 'comment'];
    public $timestamps = true;
}
