<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    protected $table = 'archive';
    protected $fillable = ['id', 'storage_id', 'offer_id', 'count'];
    public $timestamps = true;
}
