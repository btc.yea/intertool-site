@extends('front')

@section('styles')
    <style>
        .header__catalog:hover .categories__modal {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="breadcrumbs">
            <a href="/">Главная</a>
            <a href="{{route('category', $currentCategory->id)}}">{{$currentCategory->name}}</a>
            {{$product->name}}
        </div>
        <h1 class="product__name">{{$product->name}}</h1>
        <div class="rateit my-3"
            data-rateit-value="{{$avarageRating}}"
            data-rateit-ispreset="true"
            data-rateit-readonly="true"
            data-rateit-mode="font"
            style="font-size: 25px;"></div>
    </div>

    <div class="product__menu">
        <div class="container">
            <span href="#" class="btn active" data-chooses="true" data-choose="about" data-active="true">Все о товаре</span>
            <span href="#" class="btn" data-chooses="true" data-choose="params" data-active="false">Характеристики</span>
            <span href="#" class="btn" data-chooses="true" data-choose="rating" data-active="false">Отзывы ({{$ratingCount}})</span>
            <span href="#" class="btn" data-chooses="true" data-choose="video" data-active="false">Видео (0)</span>
        </div>
    </div>

    <div class="product__header">
        <div class="container">
            <div class="product__wrapper">
                <div class="product__images">
                    <div class="images__select">
                        @php
                            $i=0;
                        @endphp
                        @foreach ($productImages as $img)
                            <img src="{{asset($img)}}" alt="" id="selector">
                            @php
                                $i++;
                                if ($i === 5) {
                                    break;
                                }
                            @endphp
                        @endforeach
                    </div>
                    <img src="{{asset($productImages[0])}}" class="selected" alt="">
                </div>
                <div class="product__details">
                    <div id="params" data-choosen="true" class="d-none container-fluid">
                        <table class="table table-striped">
                            @foreach ($params as $param)
                                <tr>
                                    <td class="px-2 py-4">{{$param['name'][0] ?? ''}}</td>
                                    <td class="px-2 py-4">{!!$param['value'] ?? ''!!} {!!$param['unit'][0] ?? ''!!}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    {{-- О товаре --}}
                    <div id="about" data-choosen="true" class="px-2 my-4 container-fluid">

                        @if ($product->available == 'true')
                            <div class="available d-inline-block">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                &nbsp; Есть в наличии
                            </div>
                        @else
                            <div class="no-available d-inline-block">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                &nbsp;Нет в наличии
                            </div>
                        @endif
                        
                        <div class="price">
                            {{$product->price}} грн
                        </div>

                        <div class="d-flex justify-content-center mt-4">
                            <button class="btn btn-default">
                                <i class="fas fa-shopping-cart" aria-hidden="true"></i>
                                В корзину
                            </button>
                            <button class="btn btn-default">Купить</button>
                        </div>
                    </div>
                    {{-- Отзывы --}}
                    <div id="rating" class="d-none px-2 my-4 container-fluid">
                        <div id="js-rating"
                            data-choosen="false"
                            data-csrf="{{csrf_token()}}"
                            data-rating-count="{{$ratingCount}}"
                            data-is-guest="{{\Auth::guest()}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container about__product">
        {!!$product->about ?? ''!!}
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/product-images-slider.js')}} "></script>
@endsection