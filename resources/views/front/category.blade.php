@extends('front')

@section('content')
    <div class="container">
        <div class="breadcrumbs">
            <a href="/">Главная</a>
            @if (isset($search))
                Поиск
            @else
                {{$currentCategory->name}}
            @endif
        </div>
    </div>

    @if (isset($search))
        <div class="container alert-secondary p-4 mb-2">
            Результат по запросу "<span class="fw-bold">{{$search}}</span>"
        </div>
    @endif

    <div class="offers">
        <div class="offers__wrapper">
            @foreach ($offers as $offer)
                @if (Illuminate\Support\Facades\File::exists('storage/offers/'.$offer->model.'.jpg'))
                    <a href="{{route('product', $offer->id)}}" class="offer">
                        <div class="offer__image">
                            <img src="{{asset('storage/offers/'.$offer->model.'.jpg')}}" alt="{{$offer->name}}">
                        </div>
                        <div class="offer__body">
                            <div class="offer__name">{{$offer->name}}</div>
                            <div class="row justify-content-between align-items-end">
                                <div class="col-auto">
                                    <div class="offer__price">{{$offer->price}} грн</div>
                                </div>
                                <div class="col-auto">
                                    <div class="rateit"
                                            data-rateit-value="{{\App\Helpers\OfferHelper::getAvarageRating($offer->id) ?? 5}}"
                                            data-rateit-ispreset="true"
                                            data-rateit-readonly="true"
                                            data-rateit-mode="font"
                                            style="font-size: 14px;"></div>
                                </div>
                            </div>
                            <div class="offer__available">
                                {{$offer->available == 'true' ? '' : 'Нет в наличии'}}
                            </div>
                        </div>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
@endsection
