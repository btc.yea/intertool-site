@extends('front')

@section('content')
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            @foreach ($images as $image)
            <div class="swiper-slide">
                <img src="{{asset('slider/'.$image->image)}}" alt="" class="slide">
            </div>
            @endforeach
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="stock">
        <div class="container">
            <h2 class="stock__header text-center">Акции</h2>
        </div>
        <div class="offers">
            <div class="offers__wrapper">
                @foreach ($stocks as $stock)
                    <a href="{{route('product', $stock->id)}}" class="offer">
                        <div class="offer__image">
                            <div class="stock-percent">{{round((($stock->price - $stock->new_price) / $stock->price) * -100)}}%</div>
                            <img src="{{asset('storage/offers/'.$stock->model.'.jpg')}}" alt="{{$stock->name}}">
                        </div>
                        <div class="offer__body">
                            <div class="offer__name">{{$stock->name}}</div>
                            <div class="row justify-content-between align-items-end">
                                <div class="col-auto">
                                    <div class="offer__price text-danger">
                                        {{$stock->new_price}} грн
                                        <div class="text-decoration-line-through m-0 text-secondary fs-4">{{$stock->price}} грн</div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                        <div class="rateit"
                                            data-rateit-value="{{\App\Helpers\OfferHelper::getAvarageRating($stock->id) ?? 5}}"
                                            data-rateit-ispreset="true"
                                            data-rateit-readonly="true"
                                            data-rateit-mode="font"
                                            style="font-size: 14px;"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection