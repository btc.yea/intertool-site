<div class="categories__modal">
    <div class="categories__container">
        <div class="categories__wrapper">
            <nav class="categories__nav">
                @foreach ($categories as $cat)
                    @if (!$cat->is_hidden)
                        <a href="#" class="categories__nav-item" data-id="{{$cat->id}}">
                            {{ $cat->name }} <i class="fas fa-chevron-right"></i>
                        </a>
                    @endif
                @endforeach
            </nav>
            @foreach ($subcategories as $id => $subcats)
                <div class="categories__category" data-parent-id="{{$id}}">
                    @foreach ($subcats as $cat)
                        @if (!$cat['is_hidden'])
                            <a class="categories__item" href="{{route('category', $cat['id'])}}">
                                <div class="category__image">
                                    <img src="{{asset('categories/'.$cat['image'])}}" alt="">
                                </div>
                                <div class="category__title">
                                {{$cat['name']}}
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</div>
