<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">

    <!-- Useful meta tags -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.5/rateit.min.css" integrity="sha512-VtezewVucCf4f8ZUJWzF1Pa0kLqPwpbLU/+6ocHmUWaoPqAH9F8gKmPkVYzu2wGWQs6DYuPxijbBfti7B+46FA==" crossorigin="anonymous" />
    @yield('styles')

    <title>{{ $title }}</title>
</head>
<body>
    <header class="header">
        <div class="header__inner">
            <div class="header__wrap">
                <a href="/"><img src="{{ asset('images/logo.png') }}" alt="" class="logo"></a>
                <nav class="header__links no-mobile">
                    <a href="#" class="header__link">О нас</a>
                    <a href="#" class="header__link header__link--invert">Акции</a>
                    <a href="#" class="header__link">Блог</a>
                    <a href="#" class="header__link">Оплата и доставка</a>
                    <a href="#" class="header__link">Сотрудничество</a>
                    <li class="dropdown header__phone">
                        <div href="#" class="header__link">Клиентам <i class="fas fa-chevron-down"></i></div>
                        <ul class="submenu submenu--client">
                            <li><a href="#" class="header__link">Регистрация инструмента</a></li>
                            <li><a href="#" class="header__link">Гарантия</a></li>
                            <li><a href="#" class="header__link">Возврат товара</a></li>
                        </ul>
                    </li>
                </nav>
            </div>
            <div class="header__actions">
                <!-- <a href="#" class="header__link"><i class="fas fa-map-marker-alt"></i> Запорожье</a> -->
                <li class="dropdown header__phone no-mobile">
                    <div class="header__link header__link--m0">Контакты <i class="fas fa-chevron-down"></i></div>
                    <ul class="submenu text-center">
                        @foreach ($contacts as $tel)
                            <li>
                                <a href="tel:{{$tel->number}}" class="header__link">
                                    {{$tel->number}}
                                    <span class="text-body pt-4 px-3 fs-5 d-block lh-2r">
                                        {{$tel->worktime}}
                                    </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <div class="header__lang no-mobile">
                    <a href="#" class="header__link">Укр</a> |
                    <div class="header__link disabled">Рус</div>
                </div>
                
                <div class="burger__menu no-desktop">
                    <button class="burger__logo">
                        <span class="line"></span>
                    </button>
                    <nav class="menu__list">
                        <a href="#" class="header__link">О нас</a>
                        <a href="#" class="header__link header__link--invert">Акции</a>
                        <a href="#" class="header__link">Блог</a>
                        <a href="#" class="header__link">Оплата и доставка</a>
                        <a href="#" class="header__link">Сотрудничество</a>
                        <a href="#" class="header__link">Клиентам</a>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    
    <div class="page-header__main">
        <div class="page__container page__container--header">
            <div class="header__catalog">
                <button class="catalog__button" id='catalog-button'>
                    <i class="fab fa-buffer"></i>
                    Каталог товаров
                </button>
                @include('front.partials.categories-modal')
            </div>
        
            <div class="header-search__wrapper"
                id="js-main-search-form"
                data-action="{{ route('product.search-action') }}"
                data-csrf="{{csrf_token()}}">
                <form action="{{ route('product.search-action') }}" method="POST">
                    <input type="text" name="search" placeholder="Поиск">
                    @csrf
                    <button><i class="fas fa-search"></i></button>
                </form>
            </div>
            <div class="header__user-actions no-mobile">
                <div class="user-actions">
                    <a href="#" class="user-action" id="usermenu-button"><i class="fas fa-user"></i></a>
                        <div class="d-none user-actions__menu" id="usermenu" data-open="false">
                        @if (Auth::check())
                            <div class="text-center pb-3">
                                Добро пожаловать, {{Auth::user()->name}}!
                            </div>
                            @if (Auth::user()->hasRole('admin'))
                                <a href="{{route('admin.dashboard')}}" class="btn btn-outline-info btn-lg d-block text-dark mb-2">Админ-панель</a>
                            @endif
                            <a href="{{route('logout')}}" class="btn btn-danger btn-lg d-block">Выход</a>
                        @else
                            <div class="text-center pb-3">
                                Добро пожаловать, гость!
                            </div>
                            <a href="{{route('login')}}" class="btn btn-info btn-lg d-block mb-2 text-dark">Вход</a>
                            <a href="{{route('register')}}" class="btn btn-success btn-lg d-block text-light">Регистрация</a>
                        @endif
                    </div>
                </div>
                <a href="#" class="user-action"><i class="fas fa-shopping-cart"></i></a>
            </div>
        </div>
    </div>
    <div class="page">

        <main class="main">
            @yield('content')

            <footer class="footer">
                <div class="footer__container">
                    <div class="footer__column">
                        <div class="column__header">О Магазине</div>
                        <div class="column__body">
                            <a href="#" class="footer__link">Каталог</a>
                            <a href="#" class="footer__link">Акции</a>
                        </div>
                    </div>
                    <div class="footer__column">
                        <div class="column__header">Помощь</div>
                        <div class="column__body">
                            <a href="#" class="footer__link">Контакты</a>
                            <a href="#" class="footer__link">Возврат товара</a>
                            <a href="#" class="footer__link">Доставка и оплата</a>
                        </div>
                    </div>
                    <div class="footer__column">
                        <div class="column__header">Сервисы</div>
                        <div class="column__body">
                            <a href="#" class="footer__link">Сервис</a>
                            <a href="#" class="footer__link">Статус ремонта</a>
                        </div>
                    </div>
                    <div class="footer__column">
                        <div class="column__header">Контакты</div>
                        <div class="column__body">
                            @foreach ($contacts as $tel)
                                <a href="tel:{{$tel->number}}" class="footer__link">{{$tel->number}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </footer>
        </main>

    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/d80800fc00.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="{{ asset('js/app.js')}} "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.5/jquery.rateit.min.js" integrity="sha512-ttBgr7TNuS+00BFNY+TkWU9chna3buySaRKoA9IMmk+ueesPbUfyEsWdn5mrXB+cG+ziRdEXMHmsJjGmzBZJYQ==" crossorigin="anonymous"></script>
    @yield('scripts')
</body>
</html>
