@extends('admin')

@section('content')
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Добавить изображение для слайдера</h1>
    </div>
</div>
<form action="{{route('admin.slider.insert')}}" enctype="multipart/form-data" method="post">
    @csrf
    <label for="file" class="form-label mb-2">Выберите изображение:</label>
    <input class="form-control form-control mb-2" id="file" type="file" name="image" required />
    <button type="submit" class="btn btn app-btn-primary">Добавить</button>
</form>
@endsection