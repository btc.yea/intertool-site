@extends('admin')

@section('content')
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row g-3 mb-4 align-items-center justify-content-between mt-2">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Категории</h1>
    </div>
    <div class="col-auto">
        <a class="btn btn app-btn-primary" href="{{route('admin.categories.add')}}">Добавить</a>
    </div>
</div>
<div class="tab-content" id="orders-table-tab-content">
    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
        <div class="app-card app-card-orders-table shadow-sm mb-5">
            <div class="app-card-body">
                <div class="table-responsive">
                    <table class="table app-table-hover mb-0 text-left">
                        <thead>
                            <tr>
                                <th class="cell container-fluid">Имя категории</th>
                                <th class="cell text-center">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $cat)
                            <tr>
                                <td class="cell container-fluid">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-folder" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M9.828 4a3 3 0 0 1-2.12-.879l-.83-.828A1 1 0 0 0 6.173 2H2.5a1 1 0 0 0-1 .981L1.546 4h-1L.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3v1z"/>
                                        <path fill-rule="evenodd" d="M13.81 4H2.19a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91h10.348a1 1 0 0 0 .995-.91l.637-7A1 1 0 0 0 13.81 4zM2.19 3A2 2 0 0 0 .198 5.181l.637 7A2 2 0 0 0 2.826 14h10.348a2 2 0 0 0 1.991-1.819l.637-7A2 2 0 0 0 13.81 3H2.19z"/>
                                    </svg>
                                    {{$cat->name}}
                                </td>
                                <td class="cell">
                                    <a class="btn btn btn-danger text-white" href="{{route('admin.categories.delete', $cat->id)}}">Удалить</a>
                                </td>
                                <td class="cell">
                                    <a class="btn btn btn-warning" href="{{route('admin.categories.edit', $cat->id)}}">Изменить</a>
                                </td>
                                <td class="cell">
                                    @if ($cat->is_hidden)
                                        <a class="btn btn btn-success" href="{{route('admin.categories.toggle', $cat->id)}}">Показать</a>
                                    @else
                                        <a class="btn btn btn-danger" href="{{route('admin.categories.toggle', $cat->id)}}">Скрыть</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!--//table-responsive-->
               <div class="text-center pt-2">{{$categories->links()}}</div>
            </div><!--//app-card-body-->		
        </div><!--//app-card-->
    </div><!--//tab-pane-->
</div><!--//tab-content-->
@endsection