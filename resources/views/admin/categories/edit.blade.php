@extends('admin')

@section('content')
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Редактировать категорию</h1>
    </div>
</div>
<form action="{{route('admin.categories.edit', $id)}}" method="post" enctype="multipart/form-data">
    @csrf
    <label for="name" class="form-label mb-2">Введите имя категории:</label>
    <input class="form-control form-control mb-2" id="name" type="text" name="name" value="{{$catName}}" required />
    <label for="file" class="form-label mb-2">Выберите изображение:</label>
    <input class="form-control form-control mb-2" id="file" type="file" name="image" />
    <button type="submit" class="btn btn app-btn-primary">Сохранить</button>
</form>
@endsection