@extends('admin')

@section('content')
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Добавить категорию</h1>
    </div>
</div>
<form action="{{route('admin.categories.add')}}" method="post" enctype="multipart/form-data">
    @csrf
    <label for="cat" class="form-label mb-2">Выберите родительскую категорию:</label>
    <select class="form-select" name="parent_id" id="cat">
        <option value="1" selected>Без родительской категории</option>
        @foreach ($categories as $cat)
            <option value="{{$cat->id}}">{{$cat->name}}</option>
        @endforeach
    </select>
    <label for="name" class="form-label mb-2">Введите имя категории:</label>
    <input class="form-control form-control mb-2" id="name" type="text" name="name" required />
    <label for="file" class="form-label mb-2">Выберите изображение:</label>
    <input class="form-control form-control mb-2" id="file" type="file" name="image" required />
    <button type="submit" class="btn btn app-btn-primary">Сохранить</button>
</form>
@endsection