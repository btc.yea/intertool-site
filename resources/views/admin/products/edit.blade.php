@extends('admin')

@section('content')
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Редактировать товар</h1>
    </div>
</div>
<form action="{{route('admin.products.edit', $id)}}" method="post" enctype="multipart/form-data">
    @csrf
    <label for="name" class="form-label mb-2">Имя товара:</label>
    <input class="form-control form-control mb-2" id="name" type="text" name="name" value="{{$product->name}}" required />

    {{-- <label for="file" class="form-label mb-2">Выберите изображение:</label>
    <input class="form-control form-control mb-2" id="file" type="file" name="image" /> --}}

    <label for="model" class="form-label mb-2">Код товара:</label>
    <input class="form-control form-control mb-2" id="model" type="text" name="model" value="{{$product->model}}" required />

    <label for="about" class="form-label mb-2">Описание товара:</label>
    <textarea class="form-control form-control mb-2" id="about" type="text" name="about">{{$product->about}}</textarea>

    <label class="form-label mb-2">Характеристики:</label>
    <div class="row mb-3">
        <div class="col-4 text-center">
            Характеристика
        </div>
        <div class="col-4 text-center">
            Значение
        </div>
        <div class="col-4 text-center">
            Единица
        </div>
    </div>
    @php
        $i=0;
    @endphp
    @foreach ($params as $param)
        <div class="row">
            <div class="col-4">
                <input class="form-control form-control mb-2" type="text" name="param_name{{$i}}" value="{{$param['name'][0] ?? ''}}" />
            </div>
            <div class="col-4">
                <input class="form-control form-control mb-2" type="text" name="param_value{{$i}}" value="{!!$param['value'] ?? ''!!}" />
            </div>
            <div class="col-4">
                <input class="form-control form-control mb-2" type="text" name="param_unit{{$i}}" value="{!!$param['unit'][0] ?? ''!!}" />
            </div>
        </div>
        @php
            $i++;
        @endphp
    @endforeach
	
	<div id="append-params"></div>

    <input class="form-control form-control mb-2" type="hidden" name="params_count" value="{{$i === 0 ? 0 : $i++}}" />

	<div class="d-flex justify-content-end">
		<button type="button" class="btn app-btn-primary" id="param-button" data-iterator="{{$i === 0 ? 1 : $i++}}">+ Характеристика</button>
	</div>
    <button type="submit" class="btn app-btn-primary">Сохранить</button>
</form>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{asset('/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('js/admin/product-edit.js')}}"></script>
<script>
    ClassicEditor
			.create( document.querySelector( '#about' ), {
				toolbar: {
					items: [
						'heading',
						'|',
						'bold',
						'italic',
						'fontColor',
						'fontSize',
						'link',
						'bulletedList',
						'numberedList',
						'|',
						'alignment',
						'indent',
						'outdent',
						'|',
						'blockQuote',
						'horizontalLine',
						'insertTable',
						'undo',
						'redo'
					]
				},
				language: 'ru',
				image: {
					toolbar: [
						'imageTextAlternative',
						'imageStyle:full',
						'imageStyle:side'
					]
				},
				table: {
					contentToolbar: [
						'tableColumn',
						'tableRow',
						'mergeTableCells'
					]
				},
				licenseKey: '',
			})
			.then( editor => {
				window.editor = editor;
			})
			.catch( error => {
				console.error( 'Oops, something went wrong!' );
				console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
				console.warn( 'Build id: vse2i94i7r6z-r6nbvvta97oi' );
				console.error( error );
			});
</script>
@endsection
