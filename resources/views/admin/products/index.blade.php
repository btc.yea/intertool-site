@extends('admin')

@section('content')
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif

<div class="row g-3 mb-4 align-items-center justify-content-between mt-2">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Товары</h1>
    </div>
    <div class="col-auto">
        <a class="btn btn app-btn-primary" href="#">Добавить</a>
    </div>
</div>

<form action="{{route('admin.products.search-action')}}" method="GET">
        <select class="form-select mb-1" name="cat_id" id="cat">
            @if (isset($cat_id))
                <option value="1">Без родительской категории</option>
            @else 
                <option value="1" selected>Без родительской категории</option>
            @endif
            @foreach ($categories as $cat)
                @if (isset($cat_id) && $cat_id == $cat->id)
                    <option value="{{$cat->id}}" selected>{{$cat->name}}</option>
                @else
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                @endif
            @endforeach
        </select>
    <div class="input-group rounded pb-2">
        <input type="search" class="form-control rounded" name="search" placeholder="Введите название товара" aria-label="Введите название товара" value="{{$search ?? ''}}" />
        @csrf
        <button class="btn btn-primary btn-lg p-2">Поиск</button>
    </div>
</form>

<div class="px-2">
        Товаров на странице:
    <select class="form-select d-inline" style="width:unset" id="prodPerPage">
        <option value="15" {{$user->products_per_page === 15 ? 'selected' : ''}}>15</option>
        <option value="30" {{$user->products_per_page === 30 ? 'selected' : ''}}>30</option>
        <option value="50" {{$user->products_per_page === 50 ? 'selected' : ''}}>50</option>
        <option value="100" {{$user->products_per_page === 100 ? 'selected' : ''}}>100</option>
    </select>
</div>

@if (isset($search))
    <div class="alert alert-secondary">
        Результаты по поиску "<b>{{$search}}</b>"
    </div>
@endif

<div class="text-center d-flex justify-content-end">{{$products->appends(['search' => $search ?? '', 'cat_id' => $cat_id ?? ''])->links()}}</div>
<div class="tab-content" id="orders-table-tab-content">
    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
        <div class="app-card app-card-orders-table shadow-sm mb-5">
            <div class="app-card-body">
                <div class="table-responsive">
                    <table class="table app-table-hover mb-0 text-left">
                        <thead>
                            <tr>
                                <th class="cell container-fluid">Имя товара</th>
                                <th class="cell text-center">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td class="cell container-fluid">
                                    @if (Illuminate\Support\Facades\File::exists('storage/offers/'.$product->model.'.jpg'))
                                        <img src="{{asset('storage/offers/'.$product->model.'.jpg')}}" alt="{{$product->name}}" width="50">
                                        {{$product->name}}
                                    @else
                                        <b style="color:#eb3434">{{$product->name}}</b>
                                    @endif
                                    {!!$product->about ? '' : '<b style="color:red">!!!</b>'!!}
                                </td>
                                <td class="cell">
                                    <a class="btn btn btn-danger text-white" href="#">Удалить</a>
                                </td>
                                <td class="cell">
                                    <a class="btn btn btn-warning" href="{{route('admin.products.edit', $product->id)}}">Изменить</a>
                                </td>
                                <td class="cell">
                                    <a class="btn btn btn-warning text-nowrap" href="{{route('admin.stock.add', $product->id)}}">+ Акция</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!--//table-responsive-->
               <div class="text-center pt-2">{{$products->appends(['search' => $search ?? '', 'cat_id' => $cat_id ?? ''])->links()}}</div>
            </div><!--//app-card-body-->		
        </div><!--//app-card-->
    </div><!--//tab-pane-->
</div><!--//tab-content-->
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{asset('js/admin/product-pp.js')}}"></script>    
@endsection