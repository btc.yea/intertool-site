@extends('admin')

@section('content')
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Добавить акцию</h1>
    </div>
    <div class="p-2 bg-success fw-bold col-auto text-white">{{$offer->name}}</div>
</div>
<form action="{{route('admin.stock.add', $id)}}" method="post">
    @csrf
    <label for="new_price" class="form-label mb-2">Введите новую цену(Старая цена - <span class="text-danger fw-bold">{{$offer->price}}</span>):</label>
    <input class="form-control form-control mb-2" id="new_price" type="text" name="new_price" value="{{$stock->new_price ?? ''}}" required />
    <button type="submit" class="btn btn app-btn-primary">Сохранить</button>
</form>
@endsection