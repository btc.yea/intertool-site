@extends('admin')

@section('content')
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif

<div class="row g-3 mb-4 align-items-center justify-content-between mt-2">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Акции</h1>
    </div>
    <div class="col-auto">
        <a class="btn btn app-btn-primary" href="#">Добавить</a>
    </div>
</div>

<div class="tab-content" id="orders-table-tab-content">
    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
        <div class="app-card app-card-orders-table shadow-sm mb-5">
            <div class="app-card-body">
                <div class="table-responsive">
                    <table class="table app-table-hover mb-0 text-left">
                        <thead>
                            <tr>
                                <th class="cell container-fluid">Имя товара</th>
                                <th class="cell text-center">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td class="cell container-fluid">
                                    <img src="{{asset('storage/offers/'.$product->model.'.jpg')}}" alt="{{$product->name}}" width="50">
                                    {{$product->name}}
                                </td>
                                <td class="cell">
                                    <a class="btn btn btn-danger text-white text-nowrap" href="{{route('admin.stock.delete', $product->id)}}">Удалить акцию</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!--//table-responsive-->
               <div class="text-center pt-2">{{$products->appends(['search' => $search ?? ''])->links()}}</div>
            </div><!--//app-card-body-->		
        </div><!--//app-card-->
    </div><!--//tab-pane-->
</div><!--//tab-content-->
@endsection