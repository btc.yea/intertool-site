@extends('admin')

@section('content')
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Редактировать контакт</h1>
    </div>
</div>
<form action="{{route('admin.contacts.edit', $id)}}" method="post">
    @csrf
    <label for="number" class="form-label mb-2">Введите номер:</label>
    <input class="form-control form-control mb-2" id="number" type="text" name="number" value="{{$tel->number}}" required />
    <label for="worktime" class="form-label mb-2">Введите время работы:</label>
    <input class="form-control form-control mb-2" id="worktime" type="text" name="worktime" value="{{$tel->worktime}}" />
    <button type="submit" class="btn btn app-btn-primary">Сохранить</button>
</form>
@endsection