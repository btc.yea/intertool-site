@extends('admin')

@section('content')
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Добавить контакт</h1>
    </div>
</div>
<form action="{{route('admin.contacts.insert')}}" method="post">
    @csrf
    <label for="file" class="form-label mb-2">Введите номер:</label>
    <input class="form-control form-control mb-2" id="file" type="text" name="number" required/>
    <label for="worktime" class="form-label mb-2">Введите время работы:</label>
    <input class="form-control form-control mb-2" id="worktime" type="text" name="worktime" />
    <button type="submit" class="btn btn app-btn-primary">Добавить</button>
</form>
@endsection