@extends('admin')

@section('content')
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row g-3 mb-4 align-items-center justify-content-between mt-2">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Товары в наличии</h1>
    </div>
    <div class="col-auto">
        <a class="btn btn app-btn-primary" href="{{route('admin.archive.add-storage')}}">Добавить точку</a>
        <a class="btn btn app-btn-primary" href="{{route('admin.archive.add')}}">Добавить товар</a>
    </div>
</div>

<div class="tab-content" id="orders-table-tab-content">
    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
        <div class="app-card app-card-orders-table shadow-sm mb-5">
            <div class="app-card-body">
                <div class="table-responsive">
                    <table class="table app-table-hover mb-0 text-left">
                        <thead>
                            <tr>
                                <th class="cell">Товар</th>
                                <th class="cell">Точка</th>
                                <th class="cell">Кол-во</th>
                                <th class="cell">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($archive as $item)
                            <tr>
                                <td class="cell">
                                    @if (Illuminate\Support\Facades\File::exists('storage/offers/'.\App\Helpers\ArchiveHelper::getOfferById($item->id)->model.'.jpg'))
                                        <img src="{{asset('storage/offers/'. \App\Helpers\ArchiveHelper::getOfferById($item->id)->model.'.jpg')}}" width="50">
                                    @endif
                                    <a href="#">
                                        {{\App\Helpers\ArchiveHelper::getOfferById($item->id)->name}}
                                    </a>
                                </td>
                                <td class="cell">
                                    <a href="#">
                                        {{\App\Helpers\ArchiveHelper::getStorageNameById($item->storage_id)}}
                                    </a>
                                </td>
                                <td class="cell">
                                    {{$item->count}}
                                </td>
                                <td class="cell">
                                    <a class="btn btn btn-danger text-white" href="{{route('admin.archive.delete', $item->id)}}">Удалить</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!--//table-responsive-->
               <div class="text-center pt-2">{{$archive->links()}}</div>
            </div><!--//app-card-body-->		
        </div><!--//app-card-->
    </div><!--//tab-pane-->
</div><!--//tab-content-->

@endsection