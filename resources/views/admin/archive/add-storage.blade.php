@extends('admin')

@section('content')
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Добавить точку</h1>
    </div>
</div>
<form action="{{route('admin.archive.add-storage')}}"
        method="post" enctype="multipart/form-data"
        id="add_in_archive_form"
        autocomplete="off">
    @csrf
    <label for="type" class="form-label mb-2">Тип точки:</label>
    <select class="form-select" name="type" id="type">
        <option value="1">Склад</option>
        <option value="2" selected>Магазин</option>
        <option value="3">Место сбыта</option>
    </select>
    <label for="name" class="form-label mb-2">Название:</label>
    <input class="form-control form-control mb-2" id="name" type="text" name="name" required />
    <label for="address" class="form-label mb-2">Введите адрес:</label>
    <input class="form-control form-control mb-2" id="address" type="text" name="address" required />
    <button type="submit" class="btn btn app-btn-primary">Сохранить</button>
</form>
@endsection
