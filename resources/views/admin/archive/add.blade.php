@extends('admin')

@push('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
@endpush

@section('content')
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Добавить наличие товара</h1>
    </div>
</div>
<form action="{{route('admin.archive.add')}}"
        method="post" enctype="multipart/form-data"
        id="add_in_archive_form"
        autocomplete="off">
    @csrf
    <label for="storage_id" class="form-label mb-2">Выберите точку:</label>
    <select class="form-select" name="storage_id" id="storage_id">
        @foreach ($storages as $storage)
            <option value="{{$storage->id}}">{{$storage->name}}</option>
        @endforeach
    </select>
    <label for="barcode" class="form-label mb-2">Штрих-код товара:</label>
    <input class="form-control form-control mb-2" id="barcode" type="number" name="barcode" required />
    <label for="name" class="form-label mb-2">Введите имя или код товара:</label>
    <div class="autocomplete">
        <input class="form-control form-control mb-2" id="name" type="text" name="name" required />
    </div>
    <label for="count" class="form-label mb-2">Количество:</label>
    <input class="form-control form-control mb-2" id="count" type="number" name="count" required />
    <button type="submit" class="btn btn app-btn-primary">Сохранить</button>
</form>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('js/admin/add-in-archive.js')}}"></script>
@endsection