@extends('admin')

@section('content')
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="row g-3 mb-4 align-items-center justify-content-between">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Слайдер</h1>
    </div>
    <div class="col-auto">
        <a class="btn btn app-btn-primary" href="{{route('admin.slider.add')}}">Добавить</a>
    </div>
</div>
<div class="row g-4">
    @foreach ($images as $image)
    <div class="col-6 col-md-6 col-xl-4 col-xxl-3">
        <div class="app-card app-card-doc shadow-sm  h-100">
            <div class="app-card-thumb-holder p-3">
                <div class="app-card-thumb">
                    <img class="thumb-image" src="{{asset('slider/'.$image->image)}}" alt="">
                </div>
                 <a class="app-card-link-mask" href="#file-link"></a>
            </div>
            <div class="app-card-body p-3 has-card-actions">
                <a class="btn btn btn-danger text-white" href="{{route('admin.slider.delete', $image->id)}}">Удалить</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="row g-3 mb-4 align-items-center justify-content-between mt-2">
    <div class="col-auto">
        <h1 class="app-page-title mb-0">Контакты</h1>
    </div>
    <div class="col-auto">
        <a class="btn btn app-btn-primary" href="{{route('admin.contacts.add')}}">Добавить</a>
    </div>
</div>
<div class="tab-content" id="orders-table-tab-content">
    <div class="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
        <div class="app-card app-card-orders-table shadow-sm mb-5">
            <div class="app-card-body">
                <div class="table-responsive">
                    <table class="table app-table-hover mb-0 text-left">
                        <thead>
                            <tr>
                                <th class="cell">Номер</th>
                                <th class="cell text-center">Время работы</th>
                                <th class="cell text-center">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contacts as $tel)
                            <tr>
                                <td class="cell">{{$tel->number}}</td>
                                <td class="cell container-fluid text-center">{{$tel->worktime}}</td>
                                <td class="cell"><a class="btn btn btn-danger text-white" href="{{route('admin.contacts.delete', $tel->id)}}">Удалить</a></td>
                                <td class="cell"><a class="btn btn btn-warning" href="{{route('admin.contacts.edit', $tel->id)}}">Изменить</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!--//table-responsive-->
               
            </div><!--//app-card-body-->		
        </div><!--//app-card-->
    </div><!--//tab-pane-->
</div><!--//tab-content-->
@endsection