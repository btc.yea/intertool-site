const slider = document.getElementsByClassName('swiper-container');
if (slider.length) {
    let mainSlider = new Swiper('.swiper-container', {
        speed: 400,
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
    });    
}
