$('#param-button').click(function(e) {
    e.preventDefault();

    let $this = $(this);
    let i = $this.attr('data-iterator') * 1;

    $this.attr('data-iterator', i+1);
    $('input[name="params_count"]').val(i);

    $('#append-params').append(`
        <div class="row">
            <div class="col-4">
                <input class="form-control form-control mb-2" type="text" name="param_name${i - 1}" value="" />
            </div>
            <div class="col-4">
                <input class="form-control form-control mb-2" type="text" name="param_value${i - 1}" value="" />
            </div>
            <div class="col-4">
                <input class="form-control form-control mb-2" type="text" name="param_unit${i - 1}" value="" />
            </div>
        </div>
    `);
})