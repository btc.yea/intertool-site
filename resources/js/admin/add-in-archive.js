const axios = require('axios');

$( function() {    
    const form = $('#add_in_archive_form');

    form.submit((e) => {
        e.preventDefault();
        
        let name = $('#name').val().trim(),
            barcode = $('#barcode').val().trim(),
            count = $('#count').val().trim();

        if (name.length > 0 && barcode.length > 0 && count.length > 0) {
            form.unbind('submit').submit();
        }
    });

    let barcode = $('#barcode');

    barcode.on('keypress', (e) => {
        if (e.which === 13) {
            searchByBarcode(barcode);
        }
    })
    barcode.on('paste focusout', (e) => {
        searchByBarcode(barcode);
    })

    
    $( "#name" ).autocomplete({
        source: function (request, response) {
            $.get('/admin/archive/product-name-autocomplete/' + $('#name').val(), {
                query: request.term
            }, function (data) {
                response(data);
            });
        },
        minLength: 3,
        autoFocus: true
    }).focus(function(){            
        $(this).data("uiAutocomplete").search();
    });
});

function searchByBarcode(barcode) {
    let name = $('#name');
    axios
        .get('/admin/archive/product-name-barcode/' + barcode.val().trim())
        .then(response => {name.val(response.data)});
}