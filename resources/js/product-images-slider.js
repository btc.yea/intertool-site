var selected = $('img.selected');
var selector = $('img#selector');
updateActive()
selector.each(function () {
    $(this).mouseenter(function () {
        var sourse = $(this).attr('src');
        selected.attr('src', sourse);
        updateActive();
    })
})

function updateActive() {
    selector.each(function () {
        if ($(this).attr('src') !== selected.attr('src')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
}

var choosen = $('div[data-choosen="true"]');
var chooses = $('span[data-chooses="true"]');

chooses.each(function () {
    $(this).click(function () {
        var select = $(this).attr('data-choose');
        updateChooses()
        $(this).attr('data-active', 'true').addClass('active');
        $('div#' + select).removeClass('d-none');
    })
})

function updateChooses() {
    chooses.each(function () {
        $(this).attr('data-active', 'false').removeClass('active');
        $('div#' + $(this).attr('data-choose')).addClass('d-none');
    })
}