require('./bootstrap');
require('./slider.js')
require('./mobile-nav.js')
require('./categories-modal.js')

window.Vue = require('vue');

import MainSearchForm from './components/MainSearchForm'; // product search autocomplete
import RatingForm from './components/rating/Form'; // rating

export const ratingBus = new Vue()

const mainSearchForm = document.querySelector('#js-main-search-form'); // product search autocomplete
const ratingForm = document.querySelector('#js-rating'); // rating

// product search autocomplete
if (mainSearchForm !== null && mainSearchForm !== undefined) {
    const { action, csrf } = mainSearchForm.dataset
  
    new Vue({
        render (h) {
            return h(MainSearchForm, {
                props: {
                    action,
                    csrf,
                }
            })
      }
    }).$mount(mainSearchForm)
}

// rating
if (ratingForm !== null && ratingForm !== undefined) {
    const { csrf, ratingCount, isGuest } = ratingForm.dataset
  
    new Vue({
        render (h) {
            return h(RatingForm, {
                props: {
                    csrf,
                    ratingCount,
                    isGuest
                }
            })
      }
    }).$mount(ratingForm)
}
