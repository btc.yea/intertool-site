let cats = $('.categories__nav-item');

cats.each(function (elem, index) {
    $(this).mouseenter(function () {
        let catId = $(this).attr('data-id');
        hideSubcategories();
        $(this).addClass('active');
        $(`.categories__category[data-parent-id="${catId}"]`).css('display', 'flex');
    });
})

function hideSubcategories() {
    let subcategories = $('.categories__category');

    cats.each(function (elem, index) {
        $(this).removeClass('active');
    });
    subcategories.each(function (elem, index) {
        $(this).css('display', 'none');
    });
}

$('#usermenu-button').click(function () {
    if ($('#usermenu').attr('data-open') == 'false') {
        $('#usermenu').removeClass('d-none');
        $('#usermenu').attr('data-open', 'true');
    } else {
        $('#usermenu').addClass('d-none');
        $('#usermenu').attr('data-open', 'false');
    }
});

let pageUri = window.location.pathname;


if (pageUri.split( '/' )[1] !== 'product') {
    $('.header__catalog').hover(function(){
        $('body').css('overflow', 'hidden');
    }, function(){
        $('body').css('overflow', 'auto');
    }
    );
} else {
    $('#catalog-button').click(function () {
        let $this = $(this);
        if ($this.hasClass('open')) {
            $this.removeClass('open');
            $('body').css('overflow', 'auto');
            $('.categories__modal').css('display', 'none');
        } else {
            $this.addClass('open');
            $('body').css('overflow', 'hidden');
            $('.categories__modal').css('display', 'block');
        }
    });
}