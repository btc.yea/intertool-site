<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {
    Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {
        // dashboard, index
        Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
        // slider
        Route::get('slider/add', 'DashboardController@addImage')->name('admin.slider.add');
        Route::post('slider/insert', 'DashboardController@insertImage')->name('admin.slider.insert');
        Route::get('slider/delete/{id}', 'DashboardController@removeImage')->name('admin.slider.delete');
        // contacts
        Route::get('contacts/add', 'DashboardController@addContact')->name('admin.contacts.add');
        Route::post('contacts/insert', 'DashboardController@insertContact')->name('admin.contacts.insert');
        Route::get('contacts/delete/{id}', 'DashboardController@removeContact')->name('admin.contacts.delete');
        Route::get('contacts/edit/{id}', 'DashboardController@editContact')->name('admin.contacts.edit');
        Route::post('contacts/edit/{id}', 'DashboardController@editContact')->name('admin.contacts.edit');
        // categories, index
        Route::get('categories', 'CategoriesController@index')->name('admin.categories');
        Route::get('category/{id}', 'CategoriesController@category')->name('admin.category');
        Route::get('category/edit/{id}', 'CategoriesController@edit')->name('admin.categories.edit');
        Route::post('category/edit/{id}', 'CategoriesController@edit')->name('admin.categories.edit');
        Route::get('category/remove/{id}', 'CategoriesController@remove')->name('admin.categories.delete');
        Route::get('categories/add', 'CategoriesController@add')->name('admin.categories.add');
        Route::post('categories/add', 'CategoriesController@add')->name('admin.categories.add');
        Route::get('category/toggle/{id}', 'CategoriesController@toggle')->name('admin.categories.toggle');
        // products
        Route::get('products', 'ProductsController@index')->name('admin.products');
        Route::any('products/search', 'ProductsController@search')->name('admin.products.search-action');
        Route::get('products/edit/{id}', 'ProductsController@edit')->name('admin.products.edit');
        Route::post('products/edit/{id}', 'ProductsController@edit')->name('admin.products.edit');
        Route::get('products/products-per-page/{perPage}', 'ProductsController@changeProductsPerPage')->name('admin.products.products-per-page');
        // stock
        Route::get('stock', 'ProductsController@stock')->name('admin.stock');
        Route::get('stock/add/{id}', 'ProductsController@addStock')->name('admin.stock.add');
        Route::post('stock/add/{id}', 'ProductsController@addStock')->name('admin.stock.add');
        Route::get('stock/remove/{id}', 'ProductsController@removeStock')->name('admin.stock.delete');
        // archive
        Route::get('archive', 'ArchiveController@index')->name('admin.archive');
        Route::get('archive/add', 'ArchiveController@add')->name('admin.archive.add');
        Route::post('archive/add', 'ArchiveController@add')->name('admin.archive.add');
        Route::get('archive/add-storage', 'ArchiveController@addStorage')->name('admin.archive.add-storage');
        Route::post('archive/add-storage', 'ArchiveController@addStorage')->name('admin.archive.add-storage');
        Route::get('archive/product-name-autocomplete/{search}', 'ArchiveController@productNameAutocomplete')->name('admin.archive.product-name-autocomplete');
        Route::get('archive/product-name-barcode/{code}', 'ArchiveController@productNameByBarcode')->name('admin.archive.product-name-by-barcode');
        Route::get('archive/remove-entry/{id}', 'ArchiveController@removeArchiveEntry')->name('admin.archive.delete');
    });

    // login
    Route::get('logout', function(){
        Auth::logout();
        return Redirect::to('/');
    })->name('admin.logout');
});

Auth::routes();
Route::get('logout', function(){
    Auth::logout();
    return Redirect::to('/');
})->name('logout');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/catalog/{id}', 'CategoriesController@index')->name('category');
Route::any('/products/search', 'CategoriesController@search')->name('product.search-action');
Route::get('/product/{id}', 'ProductController@index')->name('product');


// API
Route::get('/product/autocomplete/{search}', 'ProductController@mainSearchAutocomplete')->name('product-autocomplete');
Route::post('/product/rating/insert', 'ProductController@insertRating')->name('insert-rating');
Route::any('/product/rating/get/{id}', 'ProductController@getRatingComments')->name('get-rating');
Route::get('/product/rating/is-rated/{id}', 'ProductController@isRated')->name('is-rated');
